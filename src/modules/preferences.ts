import {Displays} from "./types";
const {BrowserWindow} = require('@electron/remote');

export default class Preferences {
    static getDisplaySettings(): Displays {
        const displayStorage = localStorage.getItem('displays') || '{}';
        return JSON.parse(displayStorage);
    }

    static saveDisplaySettings(settings: Displays): void {
        localStorage.setItem('displays', JSON.stringify(settings));
        this.broadcastPreferencesChange();
    }

    private static broadcastPreferencesChange(): void {
        BrowserWindow.getAllWindows().forEach(browserWindow => {
            browserWindow.webContents.send('preferences-changed');
        });
    }
}
