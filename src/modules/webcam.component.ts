const html = document.createElement('template');
const style = document.createElement('template');

html.innerHTML = `
<video autoplay></video>
`;

style.innerHTML = `
<style>
    :host {
        display: inline-block;
    }
    video {
        height: 100%;
        width: 100%;
    }
    .mirror {
        transform: rotateY(180deg);
    }
    .fullsize {
        object-fit: fill;
    }
</style>
`;

export class Webcam extends HTMLElement {
    private currentStream;
    private video;

    constructor() {
        super();
        this.attachShadow({mode: 'open'});

        const template = html.content.cloneNode(true);
        this.shadowRoot.appendChild(template);

        const stylesheet = new CSSStyleSheet();
        // @ts-ignore
        stylesheet.replaceSync(style.content.querySelector('style').innerText);
        // @ts-ignore
        this.shadowRoot.adoptedStyleSheets = [stylesheet];

        this.video = this.shadowRoot.querySelector('video');
    }

    get device(): string {
        return this.getAttribute('device');
    }

    set device(value: string) {
        if (value) {
            this.setAttribute('device', value);
        } else {
            this.removeAttribute('device');
        }
    }

    get mirror(): boolean {
        return this.hasAttribute('mirror')
    }

    set mirror(value: boolean) {
        if (value) {
            this.setAttribute('mirror', '');
        } else {
            this.removeAttribute('mirror');
        }
    }

    get fullsize(): boolean {
        return this.hasAttribute('fullsize')
    }

    set fullsize(value: boolean) {
        if (value) {
            this.setAttribute('fullsize', '');
        } else {
            this.removeAttribute('fullsize');
        }
    }

    static get observedAttributes() {
        return ['device', 'mirror', 'fullsize'];
    }

    public attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            switch (name) {
                case 'device':
                    this.setNewStream(newValue);
                    break;
                case 'mirror':
                    const isMirror = !!newValue || newValue === '';
                    this.setMirror(isMirror);
                    break;
                case 'fullsize':
                    const isFullsize = !!newValue || newValue === '';
                    this.setFullsize(isFullsize);
                    break;
            }
        }
    }

    private setMirror(mirror) {
        if (mirror) {
            this.video.classList.add('mirror');
        } else {
            this.video.classList.remove('mirror');
        }
    }

    private setFullsize(fullSize) {
        if (fullSize) {
            this.video.classList.add('fullsize');
        } else {
            this.video.classList.remove('fullsize');
        }
    }

    private setNewStream(deviceId) {
        if (!deviceId) {
            this.stopStream();
            return;
        }

        const constraints = {
            video: {
                deviceId: deviceId
            }
        }
        navigator.mediaDevices.getUserMedia(constraints).then(stream => {
            this.stopStream();
            this.video.srcObject = stream;
            this.currentStream = stream;

            const track = stream.getVideoTracks()[0];
            track.applyConstraints({frameRate: 60, noiseSuppression: true});
        });
    }

    private stopStream() {
        if (this.currentStream) {
            this.currentStream.getTracks().forEach(track => {
                track.stop();
            });
        }
        this.video.srcObject = undefined;
    }
}

window.customElements.define('cp-webcam', Webcam);
