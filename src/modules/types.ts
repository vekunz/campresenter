export interface Displays {
    [displayId: string]: {
        connected: boolean;
        technicalName: string;
        customName: string;
    }
}
