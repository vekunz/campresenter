import {Select} from "@material/mwc-select";
import {Checkbox} from "@material/mwc-checkbox";
import {Button} from "@material/mwc-button";
import {Webcam} from "../modules/webcam.component";
import Preferences from "../modules/preferences";
import '../modules/webcam.component.ts';
import '@material/mwc-button';
import '@material/mwc-checkbox';
import '@material/mwc-formfield';
import '@material/mwc-icon-button';
import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';
import '@material/mwc-top-app-bar-fixed';
const {ipcRenderer} = require('electron');
const {BrowserWindow, screen} = require('@electron/remote');

const infoButton = document.getElementById('infoButton');
const settingsButton = document.getElementById('settingsButton');
const webcamsSelect = document.getElementById('webcams') as Select;
const displaySelect = document.getElementById('displays') as Select;
const mirrorSelect = document.getElementById('mirror') as Checkbox;
const fullSizeSelect = document.getElementById('fullSize') as Checkbox;
const changeOptionsButton = document.getElementById('changeOptionsButton') as Button;
const presentButton = document.getElementById('presentButton') as Button;
const cpWebcam = document.getElementById('output') as Webcam;

webcamsSelect.onchange = webcamChanged;
displaySelect.onchange = displayChanged;
mirrorSelect.onchange = mirrorSelectionChanged;
fullSizeSelect.onchange = fullSizeSelectionChanged;
changeOptionsButton.onclick = changeOptionsClick;
presentButton.onclick = showPresentation;
infoButton.onclick = openInfoDialog;
settingsButton.onclick = openSettingsDialog;
ipcRenderer.on('preferences-changed', applyPreferences);

let webcams;
let currentPresenterWindow;
let changeOptionsEnabled = true;

(function () {
    navigator.mediaDevices.enumerateDevices().then(devices => {
        webcams = devices.filter(d => d.kind === 'videoinput');

        for (const webcam of webcams) {
            const option = document.createElement('mwc-list-item');
            option.innerText = webcam.label;
            option.value = webcam.deviceId;

            webcamsSelect.appendChild(option);
        }

        showSelectedWebcam();
        refreshUI();
    });

    for (const display of screen.getAllDisplays()) {
        const option = document.createElement('mwc-list-item');
        option.innerText = `${display.id} (${display.bounds.width}x${display.bounds.height})`;
        option.value = `${display.id}`;

        displaySelect.appendChild(option);
    }

    applyPreferences();
})();

function webcamChanged(): void {
    refreshUI();
    showSelectedWebcam();
}

function displayChanged(): void {
    refreshUI();
}

function mirrorSelectionChanged(event: Event): void {
    cpWebcam.mirror = (event.target as HTMLInputElement).checked;
}

function fullSizeSelectionChanged(event: Event): void {
    cpWebcam.fullsize = (event.target as HTMLInputElement).checked;
}

function changeOptionsClick(): void {
    if (changeOptionsEnabled) {
        changeOptionsEnabled = false;
        changeOptionsButton.label = 'Change options';

        sendWebcamSettings();
    } else {
        changeOptionsEnabled = true;
        changeOptionsButton.label = 'Apply changes';
    }

    refreshUI();
}

function showPresentation(): void {
    if (currentPresenterWindow) {
        currentPresenterWindow.close();
        changeOptionsEnabled = true;
    } else {
        if (webcamsSelect.value) {
            openWebcamOnDisplay();
            changeOptionsEnabled = false;
        }
    }
    refreshUI();
}

function showSelectedWebcam(): void {
    cpWebcam.device = webcamsSelect.value;
}

function openWebcamOnDisplay(): void {
    const path = require("path");

    const selectedDisplay = screen.getAllDisplays().find(d => d.id + '' === displaySelect.value);

    currentPresenterWindow = new BrowserWindow({
        fullscreen: true,
        x: selectedDisplay.bounds.x,
        y: selectedDisplay.bounds.y,
        backgroundColor: '#000000',
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    require('@electron/remote').require('@electron/remote/main').enable(currentPresenterWindow.webContents);
    currentPresenterWindow.loadFile(path.join(__dirname, 'viewer.html')).then(() => {
        sendWebcamSettings();
    });

    currentPresenterWindow.on('closed', () => {
        currentPresenterWindow = null;
        changeOptionsEnabled = true;
        refreshUI();
    });
}

function refreshUI(): void {
    const presenting = !!currentPresenterWindow;

    webcamsSelect.disabled = !changeOptionsEnabled;
    displaySelect.disabled = presenting;
    mirrorSelect.disabled = !changeOptionsEnabled;
    fullSizeSelect.disabled = !changeOptionsEnabled;
    if (presenting) {
        presentButton.label = 'Stop presenting';
    } else {
        presentButton.label = 'Present';
    }

    changeOptionsButton.style.display = presenting ? null : 'none';
    presentButton.disabled = !webcamsSelect.value || !displaySelect.value;
}

function openInfoDialog(): void {
    ipcRenderer.send('open-dialog', {dialog: 'info'});
}

function openSettingsDialog(): void {
    ipcRenderer.send('open-dialog', {dialog: 'preferences'});
}

function sendWebcamSettings(): void {
    const deviceId = webcamsSelect.value;
    const mirror = mirrorSelect.checked;
    const fullSize = fullSizeSelect.checked;

    currentPresenterWindow.webContents.send('webcam-settings', {
        deviceId: deviceId,
        mirror: mirror,
        fullSize: fullSize
    });
}

function applyPreferences(): void {
    const displays = Preferences.getDisplaySettings();

    const options = Array.from(displaySelect.querySelectorAll('mwc-list-item'));
    for (const option of options) {
        const id = option.value;

        if (Object.keys(displays).includes(id)) {
            if (displays[id].customName) {
                option.innerText = displays[id].customName;
            } else {
                option.innerText = displays[id].technicalName;
            }
        }
    }
    displaySelect.layoutOptions().then();
    refreshUI();
}
