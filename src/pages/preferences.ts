import {Displays} from "../modules/types";
import Preferences from "../modules/preferences";
import '@material/mwc-icon-button';
const {screen} = require('@electron/remote');

const currentDisplaysTableBody = document.querySelector<HTMLElement>('#currentDisplays tbody');
const otherDisplaysTableBody = document.querySelector<HTMLElement>('#otherDisplays tbody');
const displayTableRowTemplate = document.getElementById('displayTableRow') as HTMLTemplateElement;
const displayCustomNameInputTemplate = document.getElementById('displayCustomNameInput') as HTMLTemplateElement;

const allDisplays: Displays = {};

(function () {
    for (const display of screen.getAllDisplays()) {
        allDisplays[`${display.id}`] = {
            connected: true,
            technicalName: `${display.id} (${display.bounds.width}x${display.bounds.height})`,
            customName: ''
        };
    }

    const displayStorage = Preferences.getDisplaySettings();
    for (const [id, value] of Object.entries(displayStorage)) {
        if (Object.keys(allDisplays).includes(id)) {
            allDisplays[id].customName = value.customName;
        } else {
            allDisplays[id] = {
                connected: false,
                technicalName: value.technicalName,
                customName: value.customName
            };
        }
    }

    buildTables();
})();

function buildTables(): void {
    currentDisplaysTableBody.innerText = '';
    otherDisplaysTableBody.innerText = '';

    for (const [id, value] of Object.entries(allDisplays)) {
        const rowFragment = displayTableRowTemplate.content.cloneNode(true) as DocumentFragment;
        rowFragment.querySelector<HTMLTableRowElement>('tr').dataset.id = id;
        rowFragment.querySelector<HTMLTableRowElement>('tr').dataset.mode = value.connected ? 'view' : 'delete';
        rowFragment.querySelector<HTMLTableRowElement>('.technicalName').innerText = value.technicalName;
        rowFragment.querySelector<HTMLTableRowElement>('.customName').innerText = value.customName;
        rowFragment.querySelector<HTMLTableRowElement>('.actionButton > *').onclick = actionButtonClick;
        rowFragment.querySelector<HTMLTableRowElement>('.actionButton > *').setAttribute('icon', value.connected ? 'edit' : 'delete')
        if (value.connected)
            currentDisplaysTableBody.appendChild(rowFragment);
        else
            otherDisplaysTableBody.appendChild(rowFragment);
    }

    if (Object.values(allDisplays).filter(v => !v.connected).length === 0) {
        document.body.classList.add('no-otherDisplays')
    }
}

function actionButtonClick(event: MouseEvent): void {
    const row = (event.target as HTMLElement).closest('tr');
    const actionButton = row.querySelector<HTMLElement>('.actionButton > *');
    const customNameCell = row.querySelector<HTMLElement>('.customName');

    const id = row.dataset.id;
    const mode = row.dataset.mode;

    if (mode === 'view') {
        row.dataset.mode = 'edit';
        actionButton.setAttribute('icon', 'save');
        const input = displayCustomNameInputTemplate.content.cloneNode(true) as DocumentFragment;
        input.querySelector<HTMLInputElement>('input').value = allDisplays[id].customName;
        customNameCell.innerText = '';
        customNameCell.appendChild(input);
        customNameCell.querySelector('input').focus();
    } else if (mode === 'edit') {
        const text = customNameCell.querySelector('input').value;
        allDisplays[id].customName = text;
        row.dataset.mode = 'view';
        actionButton.setAttribute('icon', 'edit');
        customNameCell.innerText = text;

        savePreferences();
    } else {
        delete allDisplays[id];
        row.remove();

        if (Object.values(allDisplays).filter(v => !v.connected).length === 0) {
            document.body.classList.add('no-otherDisplays')
        }

        savePreferences();
    }
}

function savePreferences(): void {
    const displayStorage = {};

    for (const [id, value] of Object.entries(allDisplays)) {
        displayStorage[id] = {
            technicalName: value.technicalName,
            customName: value.customName
        }
    }

    Preferences.saveDisplaySettings(displayStorage);
}
