import {Webcam} from "../modules/webcam.component";
import '../modules/webcam.component.ts';
const {ipcRenderer} = require('electron');

const webcam = document.getElementById('webcam') as Webcam;

ipcRenderer.on('webcam-settings', settingsChangeEvent);
document.body.onkeydown = keyEvent;

function keyEvent(event) {
    if (event.key === 'Escape') {
        window.close();
    }
}

function settingsChangeEvent(event, arg): void {
    setWebcam(arg.deviceId, arg.mirror, arg.fullSize);
}

function setWebcam(deviceId: string, mirror: boolean, fullSize: boolean): void {
    webcam.device = deviceId;
    webcam.mirror = mirror;
    webcam.fullsize = fullSize;
}
