const {shell} = require('@electron/remote');

document.querySelectorAll('a').forEach(element => {
    element.onclick = event => {
        event.preventDefault();
        shell.openExternal((event.target as HTMLLinkElement).href).then();
    }
});

document.body.onkeydown = event => {
    if (event.key === 'Escape') {
        window.close();
    }
}
