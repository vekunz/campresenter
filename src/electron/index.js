const { app, BrowserWindow, dialog, ipcMain } = require('electron');
const path = require("path");
const dialogs = require('./dialogs.js');

require('@electron/remote/main').initialize();

require('./appMenu.js');

let mainWindow;

const createWindow = () => {
    mainWindow = new BrowserWindow({
        width: 700,
        height: 452,
        fullscreenable: false,
        resizable: false,
        titleBarStyle: 'hidden',
        titleBarOverlay: {
            color: '#006e8b',
            symbolColor: '#000000'
        },
        acceptFirstMouse: true,
        show: false,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    });

    mainWindow.on('close', event => {
        handleQuit(event);
    });

    mainWindow.on('closed', () => {
        mainWindow = null;
        app.quit();
    });

    require("@electron/remote/main").enable(mainWindow.webContents);

    mainWindow.loadFile(path.join(__dirname, 'pages/main.html')).then();
};

app.whenReady().then(() => {
    createWindow();

    app.on('before-quit', event => {
        handleQuit(event);
    });

    app.on('window-all-closed', () => {
        // if (process.platform !== 'darwin') {
            app.quit();
        // }
    });

    app.on('activate', () => {
        if (!mainWindow) {
            createWindow();
        }
    });
});

let canQuit = false;
function handleQuit(event) {
    const viewerOpen = !!BrowserWindow.getAllWindows().find(win => win.webContents.getURL().endsWith('viewer.html'));

    if (!canQuit && viewerOpen) {
        const result = dialog.showMessageBoxSync(mainWindow, {
            message: 'When you close the application, the presentation will also be closed.',
            type: 'warning',
            buttons: ['Ok', 'Cancel'],
            defaultId: 1,
            cancelId: 1
        });
        if (result === 1) {
            event.preventDefault();
        } else {
            canQuit = true;
        }
    }
}

ipcMain.on('open-dialog', (event, args) => {
    switch (args.dialog) {
        case 'info':
            dialogs.openInfoDialog();
            break;
        case 'preferences':
            dialogs.openSettingsDialog();
            break;
    }
});
