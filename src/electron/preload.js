const { app } = require('@electron/remote');

window.addEventListener('DOMContentLoaded', () => {
    document.body.classList.add(`platform-${process.platform}`);

    document.querySelectorAll('.var-appName').forEach(e => e.innerText = app.name);
    document.querySelectorAll('.var-appVersion').forEach(e => e.innerText = app.getVersion());
});
