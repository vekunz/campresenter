/**
 * @type {import('vite').UserConfig}
 */
const config = {
    root: './src',
    publicDir: './electron',
    base: '',
    build: {
        emptyOutDir: true,
        outDir: '../build',

        rollupOptions: {
            input: {
                main: './pages/main.html',
                viewer: './pages/viewer.html',
                info: './pages/info.html',
                preferences: './pages/preferences.html'
            }
        }
    }
};
export default config;
