# CamPresenter

With CamPresenter you can project a connected Webcam to one of your PCs displays.

Because of some limitations, CamPresenter only shows strange IDs for your displays. In the preferences, you can define custom names for your displays.

## Build yourself

If you want to build CamPresenter locally, follow this guide. First you need to install Node.js.

Download an prepare the repository:
```
git clone https://gitlab.com/vekunz/campresenter.git
cd campresenter
npm install
```

If you just want to build the current state of CamPresenter:
```
npm run build
```

If you want to work on CamPresenter, you can use this command. This command rebuilds the code every time you change something:
```
npm run build:watch
```
> This command does not watch the /src/electron directory. If you change files in this directory you need to restart the command.

To start CamPresenter in development mode:
```
npm start
```

To create a binary for your current operating system:
```
npm run package
```

You can also build binaries for distribution with these commands:
```
npm run package:macos:x64 # -> macOS with Intel CPU (also works with Apple Silicon)
npm run package:macos:arm64 # -> macOS with Apple Silicon
npm run package:win:x64 # -> Windows with Intel or AMD CPU (also works with ARM)
npm run package:win:arm64 # -> Windows for ARM
```
> You cannot build every variant on every operating system. The macOS commands produce .dmg bundles; the Windows commands produce installers.

To build all binaries at once:
```
npm run package:all
```
> This command does not work on every operating system. In addition to the two Windows installers for x86 and ARM CPUs, this command creates a combined installer for Windows.
